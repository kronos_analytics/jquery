<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

/**
 * The version of jquery
 */
define('JQUERY_VERSION', '1_12_4_0');


/**
 * The JQUERY_COMPRESSION_xxx constants define the different types of compression
 * available for jquery files.
 *
 * @deprecated	since 1.3.2. Use Func_Jquery constants instead
 */
define('JQUERY_COMPRESSION_MINIFIED', 'minified');
define('JQUERY_COMPRESSION_PACKED', 'packed');
define('JQUERY_COMPRESSION_NONE', 'scripts');


/**
 * This functionality provides the possibility to includes jquery scripts in a page.
 */
class Func_Jquery extends bab_Functionality
{
    private $baseScriptsPath;
    private $baseStylesPath;
    private $compressionType;

    private $noConflictAdded = false;

    /**
     * The page object in which scripts will be included.
     * @var object
     */
    private $page;

    /**
     * @var string	Included files will be compressed (recommended for production).
     */
    const COMPRESSION_MINIFIED = 'minified';

    /**
     * @var string	Now equivalent to COMPRESSION_MINIFIED.
     * @deprecated	Since 1.3.2 Use COMPRESSION_MINIFIED instead
     */
    const COMPRESSION_PACKED = 'minified';

    /**
     * @var string	Included files will not be compressed (recommended for javascript debugging).
     */
    const COMPRESSION_NONE = 'scripts';


    public $deferred = false;

    function __construct()
    {
        $addon = bab_getAddonInfosInstance('jquery');

        $this->baseScriptsPath = $addon->getPhpPath();
        $this->baseStylesPath = $addon->getStylePath();

        if ('vendor' !== mb_substr($this->baseStylesPath, 0, 6)) { // can be removed after ovidentia 8.1.98
            $this->baseStylesPath = 'addons/jquery/';
        }

        if (isset($_COOKIE['bab_debug'])) {
            $this->setCompression(self::COMPRESSION_NONE);
        } else {
            $this->setCompression(self::COMPRESSION_MINIFIED);
        }
        $this->page = bab_getInstance('babBody');
    }


    /**
     * Selects the compression that will be used for included javascript files.
     *
     * The $compressionType string can be any of the following constants:
     * - JQUERY_COMPRESSION_MINIFIED
     * - JQUERY_COMPRESSION_NONE
     *
     * @param string $compressionType		One of the JQUERY_COMPRESSION_xxx constants.
     * @return string		The actually selected compression type.
     */
    function setCompression($compressionType)
    {
        switch ($compressionType) {
            case self::COMPRESSION_MINIFIED:
            case self::COMPRESSION_NONE:
                $this->compressionType = $compressionType;
                break;
        }
        return $this->compressionType;
    }


    function getDescription() {
        return 'Includes jquery scripts';
    }


    protected function useCDN()
    {
        return (!isset($GLOBALS['babUseJQueryCDN']) || (isset($GLOBALS['babUseJQueryCDN']) && $GLOBALS['babUseJQueryCDN'] == true));
    }

    /**
     * Returns the url to the specified javascript file.
     *
     * @param string $filename
     * @return string
     */
    function getJavascriptUrl($filename)
    {
        if ($this->useCDN()) {
            if ($this->compressionType === self::COMPRESSION_MINIFIED) {
                $min = '.min';
            } else {
                $min = '';
            }
            switch ($filename) {
                case 'jquery':
                    return '//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery' . $min . '.js';
                    break;
                case 'jquery-ui':
                    switch ($this->compressionType) {
                        case self::COMPRESSION_MINIFIED:
                            return $this->baseScriptsPath . 'scripts/minified/' . $filename . '.min.js';

                        case self::COMPRESSION_NONE:
                        default:
                            return $this->baseScriptsPath . 'scripts/' . $filename . '.js';
                    }
                    break;
                case 'jquery.mobile':
                    return '//code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5' . $min . '.js';
                    break;
                case 'jquery-migrate':
                    return '//code.jquery.com/jquery-migrate-1.4.1' . $min . '.js';
                    break;
            }
        } else {

            switch ($this->compressionType) {
                case self::COMPRESSION_MINIFIED:
                    return $this->baseScriptsPath . 'scripts/minified/' . $filename . '.min.js';

                case self::COMPRESSION_NONE:
                default:
                    return $this->baseScriptsPath . 'scripts/' . $filename . '.js';
            }

        }
        return '';
    }

    /**
     * Returns the url to the specified theme css file.
     *
     * @param string $theme
     * @return string
     */
    function getStyleSheetUrl($theme = '')
    {
        /*if ($this->useCDN()) {
            if ($this->compressionType === self::COMPRESSION_MINIFIED) {
                $min = '.min';
            } else {
                $min = '';
            }
            switch ($theme) {
                case 'base':
                default:
                    return 'http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css';

                case 'mobile':
                    return 'http://code.jquery.com/mobile/1.3.2/jquery.mobile-1.3.2' . $min . '.css';
            }
        }else{*/
            switch ($theme) {
                case 'base':
                default:
                    return $this->baseStylesPath . 'base/jquery-ui.css';

                case 'mobile':
                    return $this->baseStylesPath . 'mobile/jquery.mobile.css';
            }
        //}
        return '';
    }

    /**
     * Returns the url to the specified localization script for the specified language.
     *
     * @param string $lang	or null for all available languages
     * @return string
     */
    function getI18nUrl($lang = null)
    {
        if ($this->compressionType === self::COMPRESSION_MINIFIED) {
            $min = '.min';
        } else {
            $min = '';
        }
        $langA = array('en' => 'en-GB', 'fr' => 'fr');
        if ($this->useCDN()) {
            return '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/i18n/jquery.ui.datepicker-' . $langA[$lang] . $min . '.js';
        } else {
            if (isset($lang)) {
                if($min){
                    return $this->baseScriptsPath . 'scripts/minified/i18n/jquery-ui-i18n.min.js?lang='.$langA[$lang];
                }else{
                    return $this->baseScriptsPath . 'scripts/i18n/jquery-ui-i18n.js?lang='.$langA[$lang];
                }
            }
        }
    }


    /**
     * Adds the specified jquery javascript file to the current ovidentia page.
     *
     * @param string 	$filename
     * @param bool		$deferred
     * @access protected
     */
    function addJavascriptFile($filename, $deferred = null)
    {
        include_once $GLOBALS['babInstallPath'] . '/utilit/devtools.php';

        $this->page->addJavascriptFile($this->getJavascriptUrl($filename), isset($deferred) ? $deferred : $this->deferred);
    }


    function includeCore($noConflict = false)
    {
        if ($noConflict) {
            $this->addJavascriptFile('jquery');
            $this->addJavascriptFile('jquery-migrate');//for compatibility with version older than 1.9
            if (!$this->noConflictAdded) {
                $this->page->addJavascript('jQuery.noConflict();' . "\n");
                $this->noConflictAdded = true;
            }
        } else {
            $this->addJavascriptFile('jquery');
            $this->addJavascriptFile('jquery-migrate');//for compatibility with version older than 1.9
        }
    }

    function includeUi()
    {
        $this->addJavascriptFile('jquery-ui');
        $this->page->addStyleSheet($this->getStyleSheetUrl('base'));
    }


    function includeMobile()
    {
        $this->addJavascriptFile('jquery.mobile');
    }


    /**
     * @deprecated use Func_Jquery::includeUi()
     */
    function includeUiCore()
    {
        $this->addJavascriptFile('jquery-ui');
    }


    /**
     * @deprecated use Func_Jquery::includeUi()
     */
    function includeEffectsCore()
    {
        $this->addJavascriptFile('jquery-ui');
    }


    /**
     * @deprecated use Func_Jquery::includeUi()
     */
    function includeEffects()
    {
        $this->addJavascriptFile('jquery-ui');
    }





    /**
     *
     * @return string
     */
    public function getLightboxJavascriptFile()
    {
        $addon = bab_getAddonInfosInstance('jquery');
        return $addon->getPhpPath() . 'jquery-lightbox/js/jquery.lightbox.js';
    }

    /**
     *
     * @return string
     */
    public function getLightboxStyleSheet()
    {
        $addon = bab_getAddonInfosInstance('jquery');
        return $addon->getPhpPath() . 'jquery-lightbox/css/jquery.lightbox.css';
    }


    /**
     * Include lightbox
     *
     *
     * @param	Widget_Page | babBody	$page
     */
    public function includeLightbox($page = null)
    {
        if (null === $page)
        {
            $page = $this->page;
        }


        /*@var $page babBody */

        $page->addJavascriptFile($this->getLightboxJavascriptFile());
        $page->addCssStyleSheet($this->getLightboxStyleSheet());
    }

}
