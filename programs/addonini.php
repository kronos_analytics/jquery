;<?php/*

[general]
name                        ="jquery"
version                     ="1.12.4.2"
description                 ="jQuery (javascript library) package for Ovidentia"
description.fr              ="Librairie partagée fournissant les fonctionnalités de Jquery"
long_description.fr         ="README.md"
encoding                    ="UTF-8"
delete                      =1
ov_version                  ="7.0.0"
php_version                 ="5.1"
mysql_character_set_database="latin1,utf8"
addon_access_control        ="0"
addon_type                  ="LIBRARY"
author                      ="Laurent Choulette (laurent.choulette@cantico.fr)"
icon                        ="icon.png"
tags                        ="library,default"

;*/?>
